# Wallet User Interface
## Getting started
Install the modules required from node package manager
```
$ npm install
```
To run webpack and develop javascript
```
$ npm run dev
```

To run the NodeJS server on port 3000
```
$ npm run start
```

To build and run with docker
```
$ docker build -t wallet:ui .
$ docker run --net=monsternet --name=wallet_ui --rm -d -p 3000:3000 wallet:ui
```

Open a browser and go to [localhost](http://localhost:3000)

## List of Packages

| Package                         | Description                                                           |
| ------------------------------- | --------------------------------------------------------------------- |
| axios                           | Promise based HTTP client for the browser and node.js                 |
| babel-core                      | Babel compiler core.                                                  |
| babel-loader                    | Allows transpiling JavaScript files using Babel and webpack.          |
| babel-preset-env                | A Babel preset that compiles ES2015+ down to ES5                      |
| babel-preset-react              | Babel preset for all React plugins.                                   |
| handlebars                      | Logicless templating language to keep the view and the code separated |
| hapi                            | Node.js web framework.                                                |
| inert                           | Static file and directory handlers plugin for hapi.js.                |
| react                           | React makes it painless to create interactive UIs                     |
| react-dom                       | Intended to be paired with the isomorphic React                       |
| vision                          | Templates rendering plugin support for hapi.js.                       |
| webpack                         | Packs CommonJs/AMD modules for the browser.                           |
