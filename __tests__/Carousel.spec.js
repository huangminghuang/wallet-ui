import React from 'react';
import Carousel from '../src/client/app/Carousel';
import renderer from 'react-test-renderer';

describe('Carousel', () => {
  it('renders correctly', () => {
    const mock = [{
      name: 'Amazon',
      img: './img/amazon.png'
    }]

    const tree = renderer.create(
      <Carousel cards={mock} />
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
