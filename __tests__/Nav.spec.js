import React from 'react';
import Nav from '../src/client/app/Nav';
import renderer from 'react-test-renderer';

describe('Nav', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <Nav />
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
