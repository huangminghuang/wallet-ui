import React from 'react';
import Aside from '../src/client/app/Aside';
import renderer from 'react-test-renderer';

describe('Aside', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <Aside />
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
