import React from 'react';
import Container from '../src/client/app/Container';
import renderer from 'react-test-renderer';

describe('Container', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <Container />
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
