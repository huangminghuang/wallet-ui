import React from 'react';

const Reviews = () => (
  <div className="card card-outline-secondary my-4">
    <div className="card-header">
      Product Reviews
    </div>
    <div className="card-body">
      <p>Halloween Town is a fantasy world filled with citizens such as deformed monsters, ghosts, ghouls, goblins, zombies, mummies, vampires, werewolves, and witches. Jack Skellington, the "Pumpkin King" and leader of the town, leads them in organizing the annual Halloween celebrations.</p>
      <small className="text-muted">Posted by Anonymous on 3/1/17</small>
      <hr />
      <p>However, privately Jack has grown weary of the same routine year after year, and wants something new. Wandering in the woods the morning after Halloween, he stumbles across seven trees containing doors leading to towns representing various holidays, and opens a portal to Christmas Town.</p>
      <small className="text-muted">Posted by Anonymous on 3/1/17</small>
      <hr />
      <p>Awed by the unfamiliar holiday, Jack returns to Halloween Town to show the residents his findings, but they fail to grasp the idea of Christmas and compare everything to their ideas of Halloween.</p>
      <small className="text-muted">Posted by Anonymous on 3/1/17</small>
      <hr />
      <a href="#" className="btn btn-success">
        <i className="fa fa-pencil-square-o mr-2" aria-hidden="true"></i>
        Leave a Review
      </a>
    </div>
  </div>
);

export default Reviews;
