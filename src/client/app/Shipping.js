import React from 'react';

const Shipping = (props) => (
  <div className="container-fluid">
    <div className="row">
      <div className="col-6">
        <address>
          <strong>{props.wallet.first_name} {props.wallet.last_name}</strong><br />
          {props.wallet.street_address}<br />
          {props.wallet.city}, {props.wallet.state_province} {props.wallet.postal_code}<br />
        </address>
      </div>
      <div className="col-6">
      <div className="card border-dark mb-3">
          <div className="card-body text-dark">
            <p className="card-text">My Store Name</p>
            <p className="lead"><strong>Total: $24.99</strong></p>
          </div>
        </div>
        <button id="btnPay" onClick={props.clickPay} type="button" className="btn btn-primary btn-lg btn-block">Pay</button>
      </div>
    </div>
  </div>
);

export default Shipping;
