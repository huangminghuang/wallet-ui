import React from 'react';
import Modal from './Modal';
import Aside from './Aside';
import Reviews from './Reviews';

const Container = () => (
  <div className="container mt-5">
    <div className="row">
      <Aside />
      <div className="col-lg-9">
        <div className="card mt-4">
          <img className="card-img-top img-fluid" src="https://storage.googleapis.com/mc-service-mesh/gallery_movies-the-nightmare-before-christmas.jpg" alt="" />
          <div className="card-body">
            <h3 className="card-title">The Nightmare Before Christmas</h3>
            <h4>$24.99</h4>
            <p className="card-text">Jack assigns the citizens of Halloween Town Christmas-themed jobs, including singing carols, making presents, and building a sleigh to be pulled by skeletal reindeer. Sally, a beautiful rag doll woman that is secretly in love with Jack, feels that their efforts will end in disaster, but Jack dismisses her and assigns her the task of sewing him a red coat to wear.</p>
            <span className="text-info mr-3">★ ★ ★ ★ ☆</span>
             4.0 stars
          </div>
          <div className="card-footer">
           <Modal />
          </div>
        </div>
        <Reviews />
      </div>
    </div>
  </div>
);

export default Container;
