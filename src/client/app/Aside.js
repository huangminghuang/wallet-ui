import React from 'react';

function ListItem(props, key) {
  return (
    <a key={key} href="#" className="list-group-item text-secondary">
      <h3>{props.title}</h3>
      <small>{props.description}</small>
    </a>
  );
}

const list = [{
    title: 'Outdoor Decor',
    description: 'They’ll think your yard is Santa’s other workshop.'
  },{
    title: 'Sculptures',
    description: 'When you’re ready to take the next step.'
  },{
    title: 'Inflatables',
    description: 'Larger-than-life happiness for the kid in all of us.'
  }];

const Aside = () => (
  <aside className="col-lg-3">
    <h1 className="my-4">Joy that the neighbors can see</h1>
    <div className="list-group">
      {list.map((name, key) => {
        return ListItem(name, key)
      })}
    </div>
  </aside>
);

export default Aside;
